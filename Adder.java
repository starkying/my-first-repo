public class Adder {
	public static void main(String[] args) {
		System.out.println("result: " + adder(3, 7));
	}

	public static int adder(int a, int b) {
		return a + b;
	}
}
